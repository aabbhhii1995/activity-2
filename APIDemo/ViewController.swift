//
//  ViewController.swift
//  APIDemo
//
//  Created by Parrot on 2019-03-03.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import WatchConnectivity


class ViewController: UIViewController, WCSessionDelegate{
    
    var session: WCSession!
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    

    //MARK: Outlets
    
    @IBOutlet weak var messageLabel: UILabel!
    
    
    //MARK: Button
    
    @IBAction func sendMsgToWatchButton(_ sender: Any) {
        // using to send message to watch
        session.sendMessage(["a":"Msg received"], replyHandler: nil, errorHandler: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (WCSession.isSupported()) {
            print("WCSession : Yes it is supported!")
            self.session = WCSession.default
            self.session.delegate = self
            self.session.activate()
        }
        else{
            print("WCSession : No it doesn't supports!")
        }
        
    }
   
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        super.didReceiveMemoryWarning()
        // receiving message from watch
        self.messageLabel.text = message["b"]! as? String
    }
    
    

    override func didReceiveMemoryWarning() {
       
    }


}

