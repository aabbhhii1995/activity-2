//
//  InterfaceController.swift
//  APIDemo WatchKit Extension
//
//  Created by Parrot on 2019-03-03.
//  Copyright © 2019 Parrot. All rights reserved.
//  test commit

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    var session:WCSession!
    
    //MARK: Outlet
    
    
    @IBOutlet var messageLabel: WKInterfaceLabel!
    
    // MARK:Button
    @IBAction func sendMessageToPhoneButton() {
        if(WCSession.isSupported()){
            session.sendMessage(["b":"NOTE:Message successfully received by watch"], replyHandler: nil, errorHandler: nil)
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if (WCSession.isSupported()) {
            print("WCSession : Yes it is supported!")
            self.session = WCSession.default
            self.session.delegate = self
            self.session.activate()
        }
        else{
            print("WCSession : No it doesn't supports!")
        }
       
        
    }
    
    
   
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        self.messageLabel.setText(message["a"]! as? String)
    }
    
    
}
